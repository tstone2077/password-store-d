# password-store-d

Extention to password-store that allows for extracting different data points from a password file

For Example:
```bash
pass show MyPass
sup3rS3cr@tPW%

user: bob
url: http://mything.edu

# List available keys
pass d MyPass -l

# Get value of a key
pass d MyPass url
http://mything.edu

# Copy a value of a key
pass d MyPass user -c
Copied key 'user' to clipboard.
```

## Intall:

```bash
bash setup_extensions
```
