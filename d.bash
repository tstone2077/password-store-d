#!/usr/bin/env bash

# Author: Thurston Stone <tstone2077@pm.me>

readonly VERSION="1.0"

cmd_data_version() {
    cat <<-_EOF
	$PROGRAM $COMMAND $VERSION - A pass extension that access to key value pairs
	_EOF
}

cmd_data_usage() {
   cmd_data_version
   cat <<-_EOF
	Usage:
	  $PROGRAM $COMMAND [-h|--help] [-v|--version]
	  $PROGRAM $COMMAND [-c|--clip] pass-name [keyname]
	  $PROGRAM $COMMAND [-l|--list] pass-name

	Options:
	  -h, --help	Show this help message and exit
	  -v, --version	Show the current version of $COMMAND
	  -c, --clip	Copy the key's value to the clipboard
	  -l, --list	List available key values for the password entry

	Examples:
	  $PROGRAM show Path/To/Pass
	    shhhhpassword
	    username: myuser
	    secret1: supersecretvalue
	    secret2: hiddenvalue
        does not count: because spaces in key
	  $PROGRAM $COMMAND Path/To/Pass -l
	    username
	    secret1
	    secret2
	  $PROGRAM $COMMAND Path/To/Pass secret1
	    supersecretvalue
	  $PROGRAM $COMMAND Path/To/Pass secret2 -c
	    Copied secret2 for Path/To/Pass to clipboard. Will clear in 45 seconds.

	_EOF
}

cmd_data() {
    passname="$1"
	keyname="$2"

	test -z $passname && cmd_data_usage && exit 1

	local path="${1%/}"
	local passfile="$PREFIX/$passname.gpg"

	if test -n "$LIST"
	then
		$GPG -d "${GPG_OPTS[@]}" "$passfile" | grep "[^[[:space:]]]*: " | sed 's/\(^[^ \t]*\): .*/\1/'
	else
	    if test -z $keyname
        then
          value=$($GPG -d "${GPG_OPTS[@]}" "$passfile" | head -1)
          keyname="password"
        else
          if test -f $passfile
          then
            value=$($GPG -d "${GPG_OPTS[@]}" "$passfile" | grep "^$keyname: "| sed 's/^'$keyname': //' || echo "Key not found: $keyname" && exit $?)
          else
            echo "$passname" does not exist
          fi
        fi
		if test -z "$CLIP"
		then
		  echo "$value"
		else
		  clip "$value" "$keyname for $passname"
		fi
    fi
}

# Global options
CLIP=""
LIST=""

# Getopt options
small_arg="hvcl"
long_arg="help,version,clip,list"
opts="$($GETOPT -o $small_arg -l $long_arg -n "$PROGRAM $COMMAND" -- "$@")"
err=$?
eval set -- "$opts"
while true; do case $1 in
	-c|--clip) CLIP="--clip"; shift ;;
	-l|--list) LIST="--list"; shift ;;
	#-m|--multiline) MULTLINE=1; shift ;;
	-h|--help) shift; cmd_data_usage; exit 0 ;;
	-V|--version) shift; cmd_data_version; exit 0 ;;
	--) shift; break ;;
esac done

[[ $err -ne 0 ]] && cmd_data_usage && exit 1
cmd_data "$@"
